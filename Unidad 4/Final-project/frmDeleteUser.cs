﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Final_project
{
    public partial class frmDeleteUser : Form
    {
        public frmDeleteUser()
        {
            InitializeComponent();
        }

        Users us = new Users();
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmDeleteUser_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void frmDeleteUser_Load(object sender, EventArgs e)
        {
            if (frmAdmin.role == "Administrator")
            {
                txtUsername.Text = frmAdmin.userName;
                txtPwd.Text = frmAdmin.pwd;
                txtPwd2.Text = frmAdmin.pwd;
                radioBtnAdministrator.Checked = true;
            }
            else
            {
                txtUsername.Text = frmAdmin.userName;
                txtPwd.Text = frmAdmin.pwd;
                txtPwd2.Text = frmAdmin.pwd;
                radioBtnUser.Checked = true;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("This action can not be restored, are you completely sure?", "Advice!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                us.DeleteUser(frmAdmin.id);
                Close();
            }
        }
    }
}
