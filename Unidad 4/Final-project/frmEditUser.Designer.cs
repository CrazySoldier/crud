﻿namespace Final_project
{
    partial class frmEditUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditUser));
            this.pnlEditUser = new System.Windows.Forms.Panel();
            this.lblEditUser = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.lblPwd = new System.Windows.Forms.Label();
            this.txtPwd = new System.Windows.Forms.TextBox();
            this.lblPwd2 = new System.Windows.Forms.Label();
            this.txtPwd2 = new System.Windows.Forms.TextBox();
            this.lblRole = new System.Windows.Forms.Label();
            this.radioBtnAdministrator = new System.Windows.Forms.RadioButton();
            this.radioBtnUser = new System.Windows.Forms.RadioButton();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pnlEditUser.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlEditUser
            // 
            this.pnlEditUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlEditUser.Controls.Add(this.btnCancel);
            this.pnlEditUser.Controls.Add(this.btnSave);
            this.pnlEditUser.Controls.Add(this.radioBtnUser);
            this.pnlEditUser.Controls.Add(this.radioBtnAdministrator);
            this.pnlEditUser.Controls.Add(this.lblRole);
            this.pnlEditUser.Controls.Add(this.txtPwd2);
            this.pnlEditUser.Controls.Add(this.lblPwd2);
            this.pnlEditUser.Controls.Add(this.txtPwd);
            this.pnlEditUser.Controls.Add(this.lblPwd);
            this.pnlEditUser.Controls.Add(this.txtUsername);
            this.pnlEditUser.Controls.Add(this.lblUsername);
            this.pnlEditUser.Controls.Add(this.lblEditUser);
            this.pnlEditUser.Location = new System.Drawing.Point(12, 12);
            this.pnlEditUser.Name = "pnlEditUser";
            this.pnlEditUser.Size = new System.Drawing.Size(344, 490);
            this.pnlEditUser.TabIndex = 0;
            // 
            // lblEditUser
            // 
            this.lblEditUser.AutoSize = true;
            this.lblEditUser.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEditUser.Location = new System.Drawing.Point(123, 17);
            this.lblEditUser.Name = "lblEditUser";
            this.lblEditUser.Size = new System.Drawing.Size(97, 25);
            this.lblEditUser.TabIndex = 0;
            this.lblEditUser.Text = "Edit User";
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.ForeColor = System.Drawing.Color.Gray;
            this.lblUsername.Location = new System.Drawing.Point(45, 63);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(158, 21);
            this.lblUsername.TabIndex = 1;
            this.lblUsername.Text = "Username or Email*";
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(49, 87);
            this.txtUsername.MaxLength = 50;
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(246, 27);
            this.txtUsername.TabIndex = 2;
            // 
            // lblPwd
            // 
            this.lblPwd.AutoSize = true;
            this.lblPwd.ForeColor = System.Drawing.Color.Gray;
            this.lblPwd.Location = new System.Drawing.Point(45, 141);
            this.lblPwd.Name = "lblPwd";
            this.lblPwd.Size = new System.Drawing.Size(88, 21);
            this.lblPwd.TabIndex = 3;
            this.lblPwd.Text = "Password*";
            // 
            // txtPwd
            // 
            this.txtPwd.Location = new System.Drawing.Point(49, 165);
            this.txtPwd.MaxLength = 50;
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.Size = new System.Drawing.Size(246, 27);
            this.txtPwd.TabIndex = 4;
            this.txtPwd.UseSystemPasswordChar = true;
            // 
            // lblPwd2
            // 
            this.lblPwd2.AutoSize = true;
            this.lblPwd2.ForeColor = System.Drawing.Color.Gray;
            this.lblPwd2.Location = new System.Drawing.Point(45, 220);
            this.lblPwd2.Name = "lblPwd2";
            this.lblPwd2.Size = new System.Drawing.Size(150, 21);
            this.lblPwd2.TabIndex = 5;
            this.lblPwd2.Text = "Password (again)*";
            // 
            // txtPwd2
            // 
            this.txtPwd2.Location = new System.Drawing.Point(49, 244);
            this.txtPwd2.MaxLength = 50;
            this.txtPwd2.Name = "txtPwd2";
            this.txtPwd2.Size = new System.Drawing.Size(246, 27);
            this.txtPwd2.TabIndex = 6;
            this.txtPwd2.UseSystemPasswordChar = true;
            // 
            // lblRole
            // 
            this.lblRole.AutoSize = true;
            this.lblRole.Location = new System.Drawing.Point(42, 320);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(53, 21);
            this.lblRole.TabIndex = 7;
            this.lblRole.Text = "Role:*";
            // 
            // radioBtnAdministrator
            // 
            this.radioBtnAdministrator.AutoSize = true;
            this.radioBtnAdministrator.Location = new System.Drawing.Point(101, 318);
            this.radioBtnAdministrator.Name = "radioBtnAdministrator";
            this.radioBtnAdministrator.Size = new System.Drawing.Size(134, 25);
            this.radioBtnAdministrator.TabIndex = 8;
            this.radioBtnAdministrator.TabStop = true;
            this.radioBtnAdministrator.Text = "Administrator";
            this.radioBtnAdministrator.UseVisualStyleBackColor = true;
            // 
            // radioBtnUser
            // 
            this.radioBtnUser.AutoSize = true;
            this.radioBtnUser.Location = new System.Drawing.Point(241, 318);
            this.radioBtnUser.Name = "radioBtnUser";
            this.radioBtnUser.Size = new System.Drawing.Size(60, 25);
            this.radioBtnUser.TabIndex = 9;
            this.radioBtnUser.TabStop = true;
            this.radioBtnUser.Text = "User";
            this.radioBtnUser.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Lime;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(21, 396);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(139, 35);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(182, 396);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(139, 35);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmEditUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(368, 514);
            this.Controls.Add(this.pnlEditUser);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEditUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit User";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmEditUser_FormClosed);
            this.Load += new System.EventHandler(this.frmEditUser_Load);
            this.pnlEditUser.ResumeLayout(false);
            this.pnlEditUser.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlEditUser;
        private System.Windows.Forms.TextBox txtPwd2;
        private System.Windows.Forms.Label lblPwd2;
        private System.Windows.Forms.TextBox txtPwd;
        private System.Windows.Forms.Label lblPwd;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lblEditUser;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.RadioButton radioBtnUser;
        private System.Windows.Forms.RadioButton radioBtnAdministrator;
        private System.Windows.Forms.Label lblRole;
    }
}