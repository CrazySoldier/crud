﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Final_project
{
    public partial class frmProcess : Form
    {
        public frmProcess()
        {
            InitializeComponent();
        }

        Users us = new Users();
        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTimer.Text = DateTime.Now.ToLongDateString() +" " + DateTime.Now.ToLongTimeString();
        }

        private void btnPowerOn_Click(object sender, EventArgs e)
        {
            btnPowerOff.BackColor = Color.ForestGreen;
            btnPowerOn.BackColor = Color.Red;
            us.Log(Users.name, "Power on");
        }

        private void btnPowerOff_Click(object sender, EventArgs e)
        {
            btnPowerOff.BackColor = Color.Red;
            btnPowerOn.BackColor = Color.ForestGreen;
            btnClockwise.BackColor = Color.ForestGreen;
            btnCounterClockwise.BackColor = Color.ForestGreen;
            us.Log(Users.name, "Power off");
        }

        private void btnClockwise_Click(object sender, EventArgs e)
        {
            if (btnPowerOff.BackColor == Color.Red)
            {
                MessageBox.Show("Turn on the motor first!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
            else
            {
                btnClockwise.BackColor = Color.Red;
                btnCounterClockwise.BackColor = Color.ForestGreen;
                us.Log(Users.name, "Clockwise");
            }
        }

        private void btnCounterClockwise_Click(object sender, EventArgs e)
        {
            if (btnPowerOff.BackColor == Color.Red)
            {
                MessageBox.Show("Turn on the motor first!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
            else
            {
                btnClockwise.BackColor = Color.ForestGreen;
                btnCounterClockwise.BackColor = Color.Red;
                us.Log(Users.name, "Counter Clockwise");
            }
        }

        private void frmProcess_Load(object sender, EventArgs e)
        {
            us.Log(Users.name, "Log in");
            btnPowerOff.BackColor = Color.Red;
        }

        private void frmProcess_FormClosed(object sender, FormClosedEventArgs e)
        {
            us.Log(Users.name, "Logout");
            Application.Exit();
        }
    }
}
