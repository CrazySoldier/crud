﻿namespace Final_project
{
    partial class frmProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProcess));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblMotorControl = new System.Windows.Forms.Label();
            this.lblProcess = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblTimer = new System.Windows.Forms.Label();
            this.btnPowerOn = new System.Windows.Forms.Button();
            this.btnPowerOff = new System.Windows.Forms.Button();
            this.btnClockwise = new System.Windows.Forms.Button();
            this.btnCounterClockwise = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnCounterClockwise);
            this.panel1.Controls.Add(this.btnClockwise);
            this.panel1.Controls.Add(this.btnPowerOff);
            this.panel1.Controls.Add(this.btnPowerOn);
            this.panel1.Location = new System.Drawing.Point(12, 132);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(733, 242);
            this.panel1.TabIndex = 0;
            // 
            // lblMotorControl
            // 
            this.lblMotorControl.AutoSize = true;
            this.lblMotorControl.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMotorControl.ForeColor = System.Drawing.Color.ForestGreen;
            this.lblMotorControl.Location = new System.Drawing.Point(242, 18);
            this.lblMotorControl.Name = "lblMotorControl";
            this.lblMotorControl.Size = new System.Drawing.Size(273, 25);
            this.lblMotorControl.TabIndex = 1;
            this.lblMotorControl.Text = "Motor Control Application";
            // 
            // lblProcess
            // 
            this.lblProcess.AutoSize = true;
            this.lblProcess.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProcess.Location = new System.Drawing.Point(7, 100);
            this.lblProcess.Name = "lblProcess";
            this.lblProcess.Size = new System.Drawing.Size(88, 25);
            this.lblProcess.TabIndex = 2;
            this.lblProcess.Text = "Process";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblTimer
            // 
            this.lblTimer.AutoSize = true;
            this.lblTimer.Location = new System.Drawing.Point(416, 100);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(0, 21);
            this.lblTimer.TabIndex = 3;
            // 
            // btnPowerOn
            // 
            this.btnPowerOn.BackColor = System.Drawing.Color.ForestGreen;
            this.btnPowerOn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnPowerOn.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPowerOn.ForeColor = System.Drawing.Color.White;
            this.btnPowerOn.Location = new System.Drawing.Point(144, 42);
            this.btnPowerOn.Name = "btnPowerOn";
            this.btnPowerOn.Size = new System.Drawing.Size(175, 49);
            this.btnPowerOn.TabIndex = 0;
            this.btnPowerOn.Text = "Power On";
            this.btnPowerOn.UseVisualStyleBackColor = false;
            this.btnPowerOn.Click += new System.EventHandler(this.btnPowerOn_Click);
            // 
            // btnPowerOff
            // 
            this.btnPowerOff.BackColor = System.Drawing.Color.ForestGreen;
            this.btnPowerOff.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnPowerOff.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPowerOff.ForeColor = System.Drawing.Color.White;
            this.btnPowerOff.Location = new System.Drawing.Point(411, 42);
            this.btnPowerOff.Name = "btnPowerOff";
            this.btnPowerOff.Size = new System.Drawing.Size(175, 49);
            this.btnPowerOff.TabIndex = 1;
            this.btnPowerOff.Text = "Power Off";
            this.btnPowerOff.UseVisualStyleBackColor = false;
            this.btnPowerOff.Click += new System.EventHandler(this.btnPowerOff_Click);
            // 
            // btnClockwise
            // 
            this.btnClockwise.BackColor = System.Drawing.Color.ForestGreen;
            this.btnClockwise.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClockwise.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClockwise.ForeColor = System.Drawing.Color.White;
            this.btnClockwise.Location = new System.Drawing.Point(144, 143);
            this.btnClockwise.Name = "btnClockwise";
            this.btnClockwise.Size = new System.Drawing.Size(175, 49);
            this.btnClockwise.TabIndex = 2;
            this.btnClockwise.Text = "Clockwise";
            this.btnClockwise.UseVisualStyleBackColor = false;
            this.btnClockwise.Click += new System.EventHandler(this.btnClockwise_Click);
            // 
            // btnCounterClockwise
            // 
            this.btnCounterClockwise.BackColor = System.Drawing.Color.ForestGreen;
            this.btnCounterClockwise.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCounterClockwise.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCounterClockwise.ForeColor = System.Drawing.Color.White;
            this.btnCounterClockwise.Location = new System.Drawing.Point(410, 143);
            this.btnCounterClockwise.Name = "btnCounterClockwise";
            this.btnCounterClockwise.Size = new System.Drawing.Size(176, 49);
            this.btnCounterClockwise.TabIndex = 3;
            this.btnCounterClockwise.Text = "Counter Clockwise";
            this.btnCounterClockwise.UseVisualStyleBackColor = false;
            this.btnCounterClockwise.Click += new System.EventHandler(this.btnCounterClockwise_Click);
            // 
            // frmProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(757, 388);
            this.Controls.Add(this.lblTimer);
            this.Controls.Add(this.lblProcess);
            this.Controls.Add(this.lblMotorControl);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProcess";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Process";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmProcess_FormClosed);
            this.Load += new System.EventHandler(this.frmProcess_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblMotorControl;
        private System.Windows.Forms.Label lblProcess;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblTimer;
        private System.Windows.Forms.Button btnCounterClockwise;
        private System.Windows.Forms.Button btnClockwise;
        private System.Windows.Forms.Button btnPowerOff;
        private System.Windows.Forms.Button btnPowerOn;
    }
}