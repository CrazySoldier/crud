﻿namespace Final_project
{
    partial class frmNewUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNewUser));
            this.pnlNewUser = new System.Windows.Forms.Panel();
            this.lblNewUser = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.lblPwd = new System.Windows.Forms.Label();
            this.txtPwd = new System.Windows.Forms.TextBox();
            this.lblPwd2 = new System.Windows.Forms.Label();
            this.txtPwd2 = new System.Windows.Forms.TextBox();
            this.lblRole = new System.Windows.Forms.Label();
            this.radioBtnAdministrator = new System.Windows.Forms.RadioButton();
            this.radioBtnUser = new System.Windows.Forms.RadioButton();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pnlNewUser.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlNewUser
            // 
            this.pnlNewUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlNewUser.Controls.Add(this.btnCancel);
            this.pnlNewUser.Controls.Add(this.btnSave);
            this.pnlNewUser.Controls.Add(this.radioBtnUser);
            this.pnlNewUser.Controls.Add(this.radioBtnAdministrator);
            this.pnlNewUser.Controls.Add(this.lblRole);
            this.pnlNewUser.Controls.Add(this.txtPwd2);
            this.pnlNewUser.Controls.Add(this.lblPwd2);
            this.pnlNewUser.Controls.Add(this.txtPwd);
            this.pnlNewUser.Controls.Add(this.lblPwd);
            this.pnlNewUser.Controls.Add(this.txtUserName);
            this.pnlNewUser.Controls.Add(this.lblUserName);
            this.pnlNewUser.Controls.Add(this.lblNewUser);
            this.pnlNewUser.Location = new System.Drawing.Point(12, 12);
            this.pnlNewUser.Name = "pnlNewUser";
            this.pnlNewUser.Size = new System.Drawing.Size(359, 476);
            this.pnlNewUser.TabIndex = 0;
            // 
            // lblNewUser
            // 
            this.lblNewUser.AutoSize = true;
            this.lblNewUser.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNewUser.Location = new System.Drawing.Point(32, 30);
            this.lblNewUser.Name = "lblNewUser";
            this.lblNewUser.Size = new System.Drawing.Size(107, 25);
            this.lblNewUser.TabIndex = 0;
            this.lblNewUser.Text = "New User";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.Color.Gray;
            this.lblUserName.Location = new System.Drawing.Point(34, 84);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(160, 19);
            this.lblUserName.TabIndex = 1;
            this.lblUserName.Text = "Username or Email*";
            // 
            // txtUserName
            // 
            this.txtUserName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserName.Location = new System.Drawing.Point(38, 106);
            this.txtUserName.MaxLength = 50;
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(253, 27);
            this.txtUserName.TabIndex = 2;
            // 
            // lblPwd
            // 
            this.lblPwd.AutoSize = true;
            this.lblPwd.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPwd.ForeColor = System.Drawing.Color.Gray;
            this.lblPwd.Location = new System.Drawing.Point(35, 157);
            this.lblPwd.Name = "lblPwd";
            this.lblPwd.Size = new System.Drawing.Size(87, 19);
            this.lblPwd.TabIndex = 3;
            this.lblPwd.Text = "Password*";
            // 
            // txtPwd
            // 
            this.txtPwd.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPwd.Location = new System.Drawing.Point(37, 179);
            this.txtPwd.MaxLength = 50;
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.Size = new System.Drawing.Size(254, 27);
            this.txtPwd.TabIndex = 4;
            this.txtPwd.UseSystemPasswordChar = true;
            // 
            // lblPwd2
            // 
            this.lblPwd2.AutoSize = true;
            this.lblPwd2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPwd2.ForeColor = System.Drawing.Color.Gray;
            this.lblPwd2.Location = new System.Drawing.Point(33, 230);
            this.lblPwd2.Name = "lblPwd2";
            this.lblPwd2.Size = new System.Drawing.Size(150, 19);
            this.lblPwd2.TabIndex = 5;
            this.lblPwd2.Text = "Password (again)*";
            // 
            // txtPwd2
            // 
            this.txtPwd2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPwd2.Location = new System.Drawing.Point(37, 252);
            this.txtPwd2.MaxLength = 50;
            this.txtPwd2.Name = "txtPwd2";
            this.txtPwd2.Size = new System.Drawing.Size(254, 27);
            this.txtPwd2.TabIndex = 6;
            this.txtPwd2.UseSystemPasswordChar = true;
            // 
            // lblRole
            // 
            this.lblRole.AutoSize = true;
            this.lblRole.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRole.ForeColor = System.Drawing.Color.Gray;
            this.lblRole.Location = new System.Drawing.Point(36, 318);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(53, 19);
            this.lblRole.TabIndex = 7;
            this.lblRole.Text = "Role:*";
            // 
            // radioBtnAdministrator
            // 
            this.radioBtnAdministrator.AutoSize = true;
            this.radioBtnAdministrator.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioBtnAdministrator.Location = new System.Drawing.Point(91, 315);
            this.radioBtnAdministrator.Name = "radioBtnAdministrator";
            this.radioBtnAdministrator.Size = new System.Drawing.Size(134, 25);
            this.radioBtnAdministrator.TabIndex = 8;
            this.radioBtnAdministrator.TabStop = true;
            this.radioBtnAdministrator.Text = "Administrator";
            this.radioBtnAdministrator.UseVisualStyleBackColor = true;
            // 
            // radioBtnUser
            // 
            this.radioBtnUser.AutoSize = true;
            this.radioBtnUser.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioBtnUser.Location = new System.Drawing.Point(231, 315);
            this.radioBtnUser.Name = "radioBtnUser";
            this.radioBtnUser.Size = new System.Drawing.Size(60, 25);
            this.radioBtnUser.TabIndex = 9;
            this.radioBtnUser.TabStop = true;
            this.radioBtnUser.Text = "User";
            this.radioBtnUser.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Lime;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(41, 380);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(126, 33);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
<<<<<<< HEAD
<<<<<<< HEAD
<<<<<<< HEAD
<<<<<<< HEAD
=======
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
>>>>>>> US4
=======
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
>>>>>>> US6
=======
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
>>>>>>> US7
=======
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
>>>>>>> US8
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(191, 380);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(127, 33);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
<<<<<<< HEAD
<<<<<<< HEAD
<<<<<<< HEAD
<<<<<<< HEAD
=======
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
>>>>>>> US4
=======
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
>>>>>>> US6
=======
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
>>>>>>> US7
=======
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
>>>>>>> US8
            // 
            // frmNewUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(383, 500);
            this.Controls.Add(this.pnlNewUser);
            this.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmNewUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New User";
            this.pnlNewUser.ResumeLayout(false);
            this.pnlNewUser.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlNewUser;
        private System.Windows.Forms.TextBox txtPwd2;
        private System.Windows.Forms.Label lblPwd2;
        private System.Windows.Forms.TextBox txtPwd;
        private System.Windows.Forms.Label lblPwd;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblNewUser;
        private System.Windows.Forms.RadioButton radioBtnUser;
        private System.Windows.Forms.RadioButton radioBtnAdministrator;
        private System.Windows.Forms.Label lblRole;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
    }
}