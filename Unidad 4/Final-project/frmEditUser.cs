﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Final_project
{
    public partial class frmEditUser : Form
    {
        public frmEditUser()
        {
            InitializeComponent();
        }

        Users us = new Users();
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text == "" || txtPwd.Text == "" || txtPwd2.Text == "")
            {
                MessageBox.Show("Please fill all the fields", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
            else if (radioBtnAdministrator.Checked == false && radioBtnUser.Checked == false)
            {
                MessageBox.Show("Please select a role", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
            else
            {
                if (radioBtnAdministrator.Checked == true)
                {
                    us.UpdateUser(txtUsername.Text, txtPwd.Text, radioBtnAdministrator.Text);
                    Close();
                }
                else
                {
                    us.UpdateUser(txtUsername.Text, txtPwd.Text, radioBtnUser.Text);
                    Close();
                }
            }
        }

        private void frmEditUser_Load(object sender, EventArgs e)
        {
            if (frmAdmin.role == "Administrator")
            {
                txtUsername.Text = frmAdmin.userName;
                txtPwd.Text = frmAdmin.pwd;
                txtPwd2.Text = frmAdmin.pwd;
                radioBtnAdministrator.Checked = true;
            }
            else
            {
                txtUsername.Text = frmAdmin.userName;
                txtPwd.Text = frmAdmin.pwd;
                txtPwd2.Text = frmAdmin.pwd;
                radioBtnUser.Checked = true;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmEditUser_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
